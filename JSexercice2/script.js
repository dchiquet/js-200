document.addEventListener('DOMContentLoaded', () => {
	let item
	let target
	let nbItems = getRandomInt(5)+6;
	let content = createElement("div", "id", "content", "");
	let body = document.body
	let menu = createElement("div", "class",  "menu dark-grey", "");
	content.appendChild(menu);
	body.appendChild(content)

	for (var i = 1; i < nbItems; i++) {
		target = getRandomInt(2);
		item = createElement("div", "class",  "item light-grey", "");
		h2 =createElement("h2", "class",  "", i);
		item.appendChild(h2);
		span = createElement("span", "class",  "", 'cible '+(target+1));
		item.appendChild(span);
		menu.appendChild(item);

	}
	let targets = createElement("div", "class", "targets grey", "")

	let pink = createElement("div", "class", "target pink", "")
	let target1 = createElement("h3", "class", "", "cible 1")
	pink.appendChild(target1);
	let count = createElement("span", "class", "compteur", "0")
	pink.appendChild(count);
	targets.appendChild(pink);

	let red = createElement("div", "class", "target red", "")
	target2 = createElement("h3", "class", "", "cible 2")
	red.appendChild(target2);
	count = createElement("span", "class", "compteur", "0")
	red.appendChild(count);
	targets.appendChild(red);
	content.appendChild(targets);



	//	<div class="target pink">
	//		 <h3>cible 1</h3>
	//		 <span class="compteur">0</span>
	//	</div>
	//	<div class="target red">
	//		 <h3>cible 2</h3>
	//		 <span class="compteur">0</span>
	//	</div>
	//	</div>
	// </div>

	console.log(nbItems)

	function getRandomInt(max) {
		return Math.floor(Math.random() * Math.floor(max));
	}


	function createElement(element, setAttributeN, setAttributeV, html) {
		let div = document.createElement(element);
		div.setAttribute(setAttributeN, setAttributeV);
		div.innerHTML = html;
		return div
	}
	// function appendChild(where, div)
	// 	where.appendChild(div);
})

	// <div id="content"> ---- OK
	// 	<div class="menu dark-grey">---- OK
	// 		<div class="item light-grey">">---- OK
	// 				<h2>1</h2>">---- OK
	// 				<span>cible 1</span>">---- OK
	// 		</div>">---- OK
	// 		<div class="item light-grey">">---- OK
	// 				<h2>2</h2>">---- OK
	// 				<span>cible 2</span>">---- OK
	// 		</div>
	// 		<div class="item light-grey">
	// 				<h2>3</h2>
	// 				<span>cible 2</span>
	// 		</div>
	// 		<div class="item light-grey">
	// 				<h2>4</h2>
	// 				<span>cible 1</span>
	// 		</div>
	// 		<div class="item light-grey">
	// 				<h2>5</h2>
	// 				<span>cible 2</span>
	// 		</div>

	// 		</div>
	// 		<div class="targets grey">
	// 		<div class="target pink">
	// 			 <h3>cible 1</h3>
	// 			 <span class="compteur">0</span>
	// 		</div>
	// 		<div class="target red">
	// 			 <h3>cible 2</h3>
	// 			 <span class="compteur">0</span>
	// 		</div>
	// 	</div>
	// </div>