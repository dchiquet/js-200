document.addEventListener('DOMContentLoaded', () => {
	let items = document.getElementsByClassName("item");
	let i;
	let span;
	const targets = document.getElementsByClassName("target")
	let nbcible1 = 0;
	let nbcible2 = 0;

	for (i = 0; i < items.length; i++) {
		span = items[i].getElementsByTagName('span')[0];
		if((span.innerHTML) == "cible 1"){
			items[i].classList.remove('light-grey');
			items[i].classList.add("pink");
			nbcible1++;
			addcount(0, nbcible1);
		}else if ((span.innerHTML) == "cible 2") {
			items[i].classList.remove('light-grey');
			items[i].classList.add("red");
			nbcible2++;
			addcount(1, nbcible2);
		}
	}
	function addcount(target, nb) {
		targets[target].getElementsByClassName('compteur')[0].innerHTML = nb;
	}
})
// "light-grey"

// element.classList